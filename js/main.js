window.onload = function(){
    const element = document.getElementsByClassName("js--pixel");
    const meng = document.getElementsByClassName("js--meng");
    const elementSave = document.getElementsByClassName('js--savePixel');
    const spheres = document.getElementsByClassName("js--color");
    const saveButton = document.getElementById('js--save');
    const loadButton = document.getElementById('js--load');
    const cursor = document.getElementById('js--cursor');
    const greenColor = document.getElementById('js--green');
    const purpleColor = document.getElementById('js--purple');
    const orangeColor = document.getElementById('js--orange');

    const greenPHColor = document.getElementById('js--greenPH');
    const purplePHColor = document.getElementById('js--purplePH');
    const orangePHColor = document.getElementById('js--orangePH');
    const groenTekst = document.getElementById('js--groenTekst');
    const paarsTekst = document.getElementById('js--paarsTekst');
    const oranjeTekst = document.getElementById('js--oranjeTekst');

    //const music = document.getElementById('js--music');

    var brush = "white";


    console.log(element)


        greenPHColor.onmouseenter = function(){
            if(greenPHColor.getAttribute('color') == 'blue') {
              if(cursor.getAttribute('color') == 'yellow') {
                greenPHColor.setAttribute('radius', .01)
                greenColor.setAttribute('radius', .1)
                groenTekst.setAttribute('rotation', "90 90 90");
              }
            }
            else if(greenPHColor.getAttribute('color') == 'yellow') {
              if(cursor.getAttribute('color') == 'blue') {
                greenPHColor.setAttribute('radius', .01)
                greenColor.setAttribute('radius', .1)
                groenTekst.setAttribute('rotation', "90 90 90");
              }
            }
            else {
              greenPHColor.setAttribute('color', brush);
            }
        }

        purplePHColor.onmouseenter = function(){
          if(purplePHColor.getAttribute('color') == 'blue') {
            if(cursor.getAttribute('color') == 'red') {
              purplePHColor.setAttribute('radius', .01)
              purpleColor.setAttribute('radius', .1)
              paarsTekst.setAttribute('rotation', "90 90 90");
            }
          }
          else if(purplePHColor.getAttribute('color') == 'red') {
            if(cursor.getAttribute('color') == 'blue') {
              purplePHColor.setAttribute('radius', .01)
              purpleColor.setAttribute('radius', .1)
              paarsTekst.setAttribute('rotation', "90 90 90");
            }
          }
          else {
            purplePHColor.setAttribute('color', brush);
          }
        }

        orangePHColor.onmouseenter = function(){
          if(orangePHColor.getAttribute('color') == 'red') {
            if(cursor.getAttribute('color') == 'yellow') {
              orangePHColor.setAttribute('radius', .01)
              orangeColor.setAttribute('radius', .1)
              oranjeTekst.setAttribute('rotation', "90 90 90");
            }
          }
          else if(orangePHColor.getAttribute('color') == 'yellow') {
            if(cursor.getAttribute('color') == 'red') {
              orangePHColor.setAttribute('radius', .01)
              orangeColor.setAttribute('radius', .1)
              oranjeTekst.setAttribute('rotation', "90 90 90");
            }
          }
          else {
            orangePHColor.setAttribute('color', brush);
          }
        }

    for(let i = 0; i < element.length; i++){
        element[i].onmouseenter = function(){
            element[i].setAttribute('color', brush);
            //music.pause();
        };
    }

    for(let i = 0; i < spheres.length; i++){
        spheres[i].onmouseenter = function(){
            brush = spheres[i].getAttribute('color');
            cursor.setAttribute("color", brush);
            //music.play();
        };
    }

    saveButton.onmousedown = function() {
      var colorArray = [];
      saveButton.setAttribute('color', 'red');
      saveButton.setAttribute('width', '.2');
      for(let i = 0; i < element.length; i++){

        colorArray.push(element[i].getAttribute('color'));
        //elementSave[i].setAttribute("color", colorArray[i]);
      }

      for(let i = 0; i < elementSave.length; i++){

        elementSave[i].setAttribute('color', colorArray[i]);
      }
    }

    saveButton.onmouseup = function() {
      saveButton.setAttribute('color', 'green');
      saveButton.setAttribute('width', '1');
    }

    loadButton.onmousedown = function() {
      var colorArray = [];
      loadButton.setAttribute('color', 'red');
      loadButton.setAttribute('width', '.2');
      for(let i = 0; i < element.length; i++){

        colorArray.push(elementSave[i].getAttribute('color'));
        //elementSave[i].setAttribute("color", colorArray[i]);
      }

      for(let i = 0; i < element.length; i++){

        element[i].setAttribute('color', colorArray[i]);
      }
    }

    loadButton.onmouseup = function() {
      loadButton.setAttribute('color', 'green');
      loadButton.setAttribute('width', '1');
    }
}
